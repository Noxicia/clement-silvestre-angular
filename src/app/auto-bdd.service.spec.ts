import { TestBed } from '@angular/core/testing';

import { AutoBddService } from './auto-bdd.service';

describe('AutoBddService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutoBddService = TestBed.get(AutoBddService);
    expect(service).toBeTruthy();
  });
});
