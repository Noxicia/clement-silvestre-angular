import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AutoListComponent} from "./auto-list/auto-list.component";
import {AutoSaveComponent} from "./auto-save/auto-save.component";

const routes: Routes = [
  { path: 'liste', component: AutoListComponent},
  { path: 'gestion', component: AutoSaveComponent}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
