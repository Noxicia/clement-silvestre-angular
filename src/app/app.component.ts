import { Component } from '@angular/core';
import { Injectable } from '@angular/core';

import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'automobile';
  constructor() {
    const config = {
      apiKey: "AIzaSyAenzTu9IA4bbuHxaT514qDG8MZjc7NYnU",
      authDomain: "autoapp-cce91.firebaseapp.com",
      databaseURL: "https://autoapp-cce91.firebaseio.com",
      projectId: "autoapp-cce91",
      storageBucket: "autoapp-cce91.appspot.com",
      messagingSenderId: "748365164972"
    };
    firebase.initializeApp(config);
  }
}
